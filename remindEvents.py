#!/usr/bin/env python3

# ###################################################################
# Do you hate audio reminders but still want to get alerts of upcoming
# events, even if your calendar program isn't running? Sure you do!
# This code should extract events out of an ICS file of events
# and throw events coming soon (default 1 hr) to a floating window.
# it is expected to be run periodically (e.g. by cron)
# so the target has current event data. It is expected to be
# part of 5 small bits of code:
# getEvents.pl and getToDos.pl extract events from a
# Thunderbird calendar .sqlite database and write them to
# ical files (which seem compliant), then putEvents.py and
# putTodos.py extract specific events and todos based on filters
# and format them and write the foramtted blocks to myHomePage.html
# which displays them, along with the time and weather and a random
# picture as a nice home page alternative to advertising or blank.
#
# Written largely by claude.ai with some inexpert human help by me.
#
# SPDX-FileCopyrightText: © 2024 David Gessel <dg@brt.llc>
# SPDX-License-Identifier: BSD-3-Clause"
# ###################################################################

import icalendar
from datetime import datetime, timedelta
from dateutil.rrule import *
from tzlocal import get_localzone
from dateutil.parser import parse
import re
import tkinter as tk
from tkinter import ttk, font
import os
import psutil
import sys

def set_top_without_focus(window):
    window.update_idletasks()  # Ensure the window is created
    window_id = window.wm_frame().winfo_id()
    os.system(f"wmctrl -i -r {window_id} -b add,above")

def kill_existing_instance():
    current_pid = os.getpid()
    current_process = psutil.Process(current_pid)
    for process in psutil.process_iter(['pid', 'name', 'cmdline']):
        if process.info['name'] == current_process.name() and \
           process.info['cmdline'] == current_process.cmdline() and \
           process.pid != current_pid:
            process.terminate()
            process.wait()

def get_events_from_ics(ics_string, window_start, window_end):
    events = []

    def append_event(e):
        start_dt = e['startdt']
        end_dt = e['enddt']
        if not start_dt.tzinfo:
            start_dt = get_localzone().localize(start_dt)
        if end_dt and not end_dt.tzinfo:
            end_dt = get_localzone().localize(end_dt)
        # Check if the event starts within our window
        if window_start <= start_dt <= window_end:
            events.append(e)

    def get_recurrent_datetimes(recur_rule, start, exclusions, all_day=False):
        rules = rruleset()

        # Parse the UNTIL value from the recurrence rule
        until_value = None
        if 'UNTIL' in recur_rule:
            until_parts = recur_rule.split(';')
            for part in until_parts:
                if part.startswith('UNTIL='):
                    until_value = parse(part[6:])
                    break

        #print(f"Recurrence Rule: {recur_rule}")
        #print(f"DTSTART: {start}")
        #print(f"UNTIL value: {until_value}")

        # Convert the UNTIL value to UTC if DTSTART is timezone-aware
        if until_value:
            if start.tzinfo:
                until_value = until_value.astimezone(start.tzinfo)

            # Handle all-day event case for UNTIL value
            if all_day:
                until_value = datetime.combine(until_value.date(), datetime.min.time())
                until_value = until_value.replace(tzinfo=start.tzinfo)

            #print(f"Modified UNTIL value: {until_value}")
            recur_rule = re.sub(r'UNTIL=\d+T?\d*Z?', 'UNTIL=' + until_value.strftime('%Y%m%dT%H%M%SZ'), recur_rule)
            #print(f"Modified Recurrence Rule: {recur_rule}")

        first_rule = rrulestr(recur_rule, dtstart=start)
        rules.rrule(first_rule)

        if not isinstance(exclusions, list):
            exclusions = [exclusions]
        for xdt in exclusions:
            try:
                rules.exdate(xdt.dt)
            except AttributeError:
                pass
        dates = []
        for d in rules.between(window_start, window_end, inc=True):
            dates.append(d)
        return dates

    cal = filter(lambda c: c.name == 'VEVENT',
                 icalendar.Calendar.from_ical(ics_string).walk()
                 )

    def date_to_datetime(d, timezone):
        return timezone.localize(datetime(d.year, d.month, d.day))

    for vevent in cal:
        summary = str(vevent.get('summary'))
        description = str(vevent.get('description'))
        location = str(vevent.get('location'))
        rawstartdt = vevent.get('dtstart').dt
        rawenddt = vevent.get('dtend').dt
        allday = False
        if not isinstance(rawstartdt, datetime):
            allday = True
            startdt = date_to_datetime(rawstartdt, get_localzone())
            if rawenddt:
                enddt = date_to_datetime(rawenddt, get_localzone())
            else:
                enddt = None
        else:
            startdt = rawstartdt
            enddt = rawenddt
            if not startdt.tzinfo:
                startdt = get_localzone().localize(startdt)
            if enddt and not enddt.tzinfo:
                enddt = get_localzone().localize(enddt)
        exdate = vevent.get('exdate')
        if vevent.get('rrule'):
            reoccur = vevent.get('rrule').to_ical().decode('utf-8')
            for d in get_recurrent_datetimes(reoccur, startdt, exdate, allday):
                if d >= window_start and d <= window_end:
                    new_e = {
                        'startdt': d,
                        'allday': allday,
                        'summary': summary,
                        'desc': description,
                        'loc': location
                    }
                    if enddt:
                        new_e['enddt'] = d + (enddt-startdt)
                    append_event(new_e)
        else:
            append_event({
                'startdt': startdt,
                'enddt': enddt,
                'allday': allday,
                'summary': summary,
                'desc': description,
                'loc': location
            })

    events.sort(key=lambda e: e['startdt'])
    return events

def display_events(events, auto_close_minutes, bg_color="#FFFF82"):
    if not events:
        return

    root = tk.Tk()
    root.withdraw()  # Hide the window initially
    root.title("Upcoming Events")
    root.configure(bg=bg_color)

    # Increase font size and make it bold
    default_font = font.nametofont("TkDefaultFont")
    default_font.configure(size=int(default_font.cget("size") * 2), weight="bold")

    style = ttk.Style(root)
    style.configure("TFrame", background=bg_color)
    style.configure("TLabel", background=bg_color, font=default_font)
    style.configure("TButton", font=default_font)

    frame = ttk.Frame(root, padding="20", style="TFrame")
    frame.grid(row=0, column=0, sticky=(tk.W, tk.E, tk.N, tk.S))

    local_tz = get_localzone()

    for i, event in enumerate(events):
        summary = event['summary']
        # Convert start time to local time zone
        start_time = event['startdt'].astimezone(local_tz).strftime('%H:%M')
        if event['allday']:
            time_str = "All Day"
        else:
            time_str = start_time

        ttk.Label(frame, text=f"{time_str} - {summary}", style="TLabel").grid(row=i, column=0, sticky=tk.W, pady=5)

    close_button = ttk.Button(frame, text="Close", command=root.quit, style="TButton")
    close_button.grid(row=len(events), column=0, pady=20)

    # Auto-close function
    def auto_close():
        root.quit()

    # Schedule auto-close
    root.after(auto_close_minutes * 60 * 1000, auto_close)

    # Show the window without stealing focus
    root.deiconify()
    
    # Set the window to the top without stealing focus
    root.after(0, lambda: set_top_without_focus(root))

    root.mainloop()
    root.destroy()

def main():
    # Kill existing instance if running
    kill_existing_instance()

    # Get the local timezone
    local_tz = get_localzone()

    # Set the time range
    now = datetime.now(local_tz)
    after_start = 10  # minutes - Change this value to adjust how long after an event starts it should still be included
    window_start = now - timedelta(minutes=after_start)
    window_end = now + timedelta(hours=1)  # Change this value to adjust the forward search range

    # Read the iCalendar file
    with open('/tmp/myCal.ics', 'r', encoding='latin-1') as file:
        ics_string = file.read()

    # Get upcoming events
    upcoming_events = get_events_from_ics(ics_string, window_start, window_end)

    # Set auto-close time (in minutes)
    auto_close_minutes = 10  # Change this value to adjust the auto-close time

    # Set the reminder window background color (default is #FFFF82)
    bg_color = "#FFFF82"

    # Display events
    if upcoming_events:
        display_events(upcoming_events, auto_close_minutes, bg_color)

if __name__ == "__main__":
    main()