#!/usr/bin/env python3

# ###################################################################
# This code should extract ToDos out of an ICS file of ToDos
# and insert them in a text file between two '\u2060' 
# delimiters, it is expected to be run periodically (e.g. by cron)
# so the target has current todo data. It is expected to be
# part of 4 small bits of code:
# getEvents.pl and getToDos.pl extract events from a 
# Thunderbird calendar .sqlite database and write them to
# ical files (which seem compliant), then putEvents.py and
# putTodos.py extract specific events and todos based on filters
# and format them and write the foramtted blocks to myHomePage.html
# which displays them, along with the time and weather and a random
# picture as a nice home page alternative to advertising or blank.
#
# Written largely by claude.ai with some inexpert human help by me.
#
# SPDX-FileCopyrightText: © 2024 David Gessel <dg@brt.llc>
# SPDX-License-Identifier: BSD-3-Clause"
# ###################################################################

from datetime import date, datetime, timedelta
import icalendar
from dateutil.rrule import *
import os
from tzlocal import get_localzone
from dateutil.parser import parse
import re

# Filter Events Parameters Variables
todo_entry_limit = 1  # Include todos with DTSTART/todo_entry_date before 'today + 1'
todo_priority_limit = 5  # Include todos with PRIORITY less than or equal to 5
todo_status_exclude = ['COMPLETED']  # Exclude todos with these STATUS values
todo_categories = []  # Include all categories if empty, or specify categories to include

def get_events_from_ics(ics_string, window_start, window_end, component_type):
    events = []  # Initialize the events list

    def append_event(e):
        start_dt = e['startdt']
        end_dt = e.get('enddt')  # Use get() to handle missing 'enddt' key
        if start_dt and not start_dt.tzinfo:
            start_dt = get_localzone().localize(start_dt)
        if end_dt and not end_dt.tzinfo:
            end_dt = get_localzone().localize(end_dt)
        if start_dt and start_dt > window_end:
            return
        # if end_dt and end_dt < window_start:
        #     return
        events.append(e)

    def get_recurrent_datetimes(recur_rule, start, exclusions, all_day=False):
        rules = rruleset()

        # Parse the UNTIL value from the recurrence rule
        until_value = None
        if 'UNTIL' in recur_rule:
            until_parts = recur_rule.split(';')
            for part in until_parts:
                if part.startswith('UNTIL='):
                    until_value = parse(part[6:])
                    break

        # Convert the UNTIL value to UTC if DTSTART is timezone-aware
        if until_value:
            if start and start.tzinfo:
                until_value = until_value.astimezone(start.tzinfo)

            # Handle all-day event case for UNTIL value
            if all_day:
                until_value = datetime.combine(until_value.date(), datetime.min.time())
                if start:
                    until_value = until_value.replace(tzinfo=start.tzinfo)

            recur_rule = re.sub(r'UNTIL=\d+T?\d*Z?', 'UNTIL=' + until_value.strftime('%Y%m%dT%H%M%SZ'), recur_rule)

        first_rule = rrulestr(recur_rule, dtstart=start)
        rules.rrule(first_rule)

        if not isinstance(exclusions, list):
            exclusions = [exclusions]
        for xdt in exclusions:
            try:
                rules.exdate(xdt.dt)
            except AttributeError:
                pass
        dates = []
        for d in rules.between(window_start, window_end, inc=True):
            dates.append(d)
        return dates

    cal = filter(lambda c: c.name == component_type,
                 icalendar.Calendar.from_ical(ics_string).walk()
                 )

    def date_to_datetime(d, timezone):
        return timezone.localize(datetime(d.year, d.month, d.day))

    for component in cal:
        summary = str(component.get('summary'))
        description = str(component.get('description'))
        location = str(component.get('location'))
        rawstartdt = component.get('dtstart')
        rawenddt = component.get('due') if component_type == 'VTODO' else component.get('dtend')
        priority = component.get('priority')
        status = str(component.get('status'))
        uid = str(component.get('uid'))  # Add this line to extract the UID
        allday = False
        if rawstartdt:
            rawstartdt = rawstartdt.dt
            if not isinstance(rawstartdt, datetime):
                allday = True
                startdt = date_to_datetime(rawstartdt, get_localzone())
            else:
                startdt = rawstartdt
                if not startdt.tzinfo:
                    startdt = get_localzone().localize(startdt)
        else:
            startdt = None
        if rawenddt:
            rawenddt = rawenddt.dt
            if not isinstance(rawenddt, datetime):
                enddt = date_to_datetime(rawenddt, get_localzone())
            else:
                enddt = rawenddt
                if not enddt.tzinfo:
                    enddt = get_localzone().localize(enddt)
        else:
            enddt = None
        exdate = component.get('exdate')
        if component.get('rrule'):
            reoccur = component.get('rrule').to_ical().decode('utf-8')
            for d in get_recurrent_datetimes(reoccur, startdt, exdate, allday):
                if d >= window_start and d <= window_end:
                    new_e = {
                        'startdt': d,
                        'allday': allday,
                        'summary': summary,
                        'desc': description,
                        'loc': location,
                        'priority': priority,
                        'status': status,
                        'uid': uid  # Add this line to include the UID in the event dictionary
                    }
                    if enddt:
                        new_e['enddt'] = d + (enddt-startdt)
                    append_event(new_e)
        else:
            new_e = {
                'startdt': startdt,
                'allday': allday,
                'summary': summary,
                'desc': description,
                'loc': location,
                'priority': priority,
                'status': status,
                'uid': uid  # Add this line to include the UID in the event dictionary
            }
            if enddt:
                new_e['enddt'] = enddt
            append_event(new_e)

    max_datetime = datetime.max.replace(tzinfo=get_localzone())
    events.sort(key=lambda e: e['startdt'] if e['startdt'] else max_datetime)
    return events

# Get today's date
today = date.today()

# Get the local timezone
local_tz = get_localzone()

# Convert today's date to a datetime object with time set to 00:00:00 and timezone set to local timezone
today_start = local_tz.localize(datetime.combine(today, datetime.min.time()))

# Read the iCalendar file for todos
with open('/tmp/myToDos.ics', 'r', encoding='latin-1') as file:
    todo_ics_string = file.read()

# Get todo events within the specified limits and categories
todo_events = get_events_from_ics(todo_ics_string, today_start - timedelta(days=todo_entry_limit), today_start + timedelta(days=365), 'VTODO')
filtered_todo_events = [
    event for event in todo_events
    if (not event['startdt'] or event['startdt'].date() <= today + timedelta(days=todo_entry_limit))
    and ('priority' in event and event['priority'] is not None and event['priority'] <= todo_priority_limit)
    and (not event['status'] or event['status'] not in todo_status_exclude)
    and (not todo_categories or any(category in event['desc'] for category in todo_categories))
]

# Format todo events
todo_events_data = "Todo:\n"
processed_uids = set()
for event in filtered_todo_events:
    uid = event['uid']
    if uid in processed_uids:
        continue
    processed_uids.add(uid)
    summary = event['summary']
    if 'enddt' in event:
        if event['enddt'] < today_start:
            todo_events_data += f"{summary} (past due)\n"
        else:
            due_date = event['enddt'].astimezone(local_tz).strftime('%Y-%m-%d')
            todo_events_data += f"{summary} due: {due_date}\n"
    else:
        todo_events_data += f"{summary}\n"

# Get the user's home directory path
home_dir = os.path.expanduser('~')

# Read the HTML file
html_file_path = os.path.join(home_dir, '.myHomePage', 'myHomePage.html')
with open(html_file_path, 'r') as file:
    html_content = file.read()

# Replace the content between the zero-width word joiner delimiters for todos
start_delimiter = '\u2060'
end_delimiter = '\u2060'
start_index = html_content.find(start_delimiter)
end_index = html_content.find(end_delimiter, start_index + 1)

if start_index != -1 and end_index != -1:
    html_content = html_content[:start_index] + start_delimiter + todo_events_data + end_delimiter + html_content[end_index + 1:]
else:
    print("Error: Zero-width word joiner delimiters not found in the HTML file for todos.")

# Write the updated HTML content back to the file
with open(html_file_path, 'w') as file:
    file.write(html_content)