#!/usr/bin/env python3

# ###################################################################
# This code should extract events out of an ICS file of events
# and insert them in a text file between two '\u200b' 
# delimiters, it is expected to be run periodically (e.g. by cron)
# so the target has current event data. It is expected to be
# part of 4 small bits of code:
# getEvents.pl and getToDos.pl extract events from a 
# Thunderbird calendar .sqlite database and write them to
# ical files (which seem compliant), then putEvents.py and
# putTodos.py extract specific events and todos based on filters
# and format them and write the foramtted blocks to myHomePage.html
# which displays them, along with the time and weather and a random
# picture as a nice home page alternative to advertising or blank.
#
# Written largely by claude.ai with some inexpert human help by me.
#
# SPDX-FileCopyrightText: © 2024 David Gessel <dg@brt.llc>
# SPDX-License-Identifier: BSD-3-Clause"
# ###################################################################

from datetime import date, datetime, timedelta
import icalendar
from dateutil.rrule import *
import os
from tzlocal import get_localzone
from dateutil.parser import parse
import re

def get_events_from_ics(ics_string, window_start, window_end):
    events = []

    def append_event(e):
        start_dt = e['startdt']
        end_dt = e['enddt']
        if not start_dt.tzinfo:
            start_dt = get_localzone().localize(start_dt)
        if end_dt and not end_dt.tzinfo:
            end_dt = get_localzone().localize(end_dt)
        if start_dt > window_end:
            return
        if end_dt:
            if end_dt < window_start:
                return
        events.append(e)

    def get_recurrent_datetimes(recur_rule, start, exclusions, all_day=False):
        rules = rruleset()
        
        # Parse the UNTIL value from the recurrence rule
        until_value = None
        if 'UNTIL' in recur_rule:
            until_parts = recur_rule.split(';')
            for part in until_parts:
                if part.startswith('UNTIL='):
                    until_value = parse(part[6:])
                    break
        
        #print(f"Recurrence Rule: {recur_rule}")
        #print(f"DTSTART: {start}")
        #print(f"UNTIL value: {until_value}")
        
        # Convert the UNTIL value to UTC if DTSTART is timezone-aware
        if until_value:
            if start.tzinfo:
                until_value = until_value.astimezone(start.tzinfo)
            
            # Handle all-day event case for UNTIL value
            if all_day:
                until_value = datetime.combine(until_value.date(), datetime.min.time())
                until_value = until_value.replace(tzinfo=start.tzinfo)
            
            #print(f"Modified UNTIL value: {until_value}")
            recur_rule = re.sub(r'UNTIL=\d+T?\d*Z?', 'UNTIL=' + until_value.strftime('%Y%m%dT%H%M%SZ'), recur_rule)
            #print(f"Modified Recurrence Rule: {recur_rule}")
        
        first_rule = rrulestr(recur_rule, dtstart=start)
        rules.rrule(first_rule)
        
        if not isinstance(exclusions, list):
            exclusions = [exclusions]
        for xdt in exclusions:
            try:
                rules.exdate(xdt.dt)
            except AttributeError:
                pass
        dates = []
        for d in rules.between(window_start, window_end, inc=True):
            dates.append(d)
        return dates

    cal = filter(lambda c: c.name == 'VEVENT',
                 icalendar.Calendar.from_ical(ics_string).walk()
                 )

    def date_to_datetime(d, timezone):
        return timezone.localize(datetime(d.year, d.month, d.day))

    for vevent in cal:
        summary = str(vevent.get('summary'))
        description = str(vevent.get('description'))
        location = str(vevent.get('location'))
        rawstartdt = vevent.get('dtstart').dt
        rawenddt = vevent.get('dtend').dt
        allday = False
        if not isinstance(rawstartdt, datetime):
            allday = True
            startdt = date_to_datetime(rawstartdt, get_localzone())
            if rawenddt:
                enddt = date_to_datetime(rawenddt, get_localzone())
            else:
                enddt = None
        else:
            startdt = rawstartdt
            enddt = rawenddt
            if not startdt.tzinfo:
                startdt = get_localzone().localize(startdt)
            if enddt and not enddt.tzinfo:
                enddt = get_localzone().localize(enddt)
        exdate = vevent.get('exdate')
        if vevent.get('rrule'):
            reoccur = vevent.get('rrule').to_ical().decode('utf-8')
            for d in get_recurrent_datetimes(reoccur, startdt, exdate, allday):
                if d >= window_start and d <= window_end:
                    new_e = {
                        'startdt': d,
                        'allday': allday,
                        'summary': summary,
                        'desc': description,
                        'loc': location
                    }
                    if enddt:
                        new_e['enddt'] = d + (enddt-startdt)
                    append_event(new_e)
        else:
            append_event({
                'startdt': startdt,
                'enddt': enddt,
                'allday': allday,
                'summary': summary,
                'desc': description,
                'loc': location
            })

    events.sort(key=lambda e: e['startdt'])
    return events

# Get today's and tomorrow's dates
today = date.today()
tomorrow = today + timedelta(days=1)

# Get the local timezone
local_tz = get_localzone()

# Convert dates to datetime objects with time set to 00:00:00 and timezone set to local timezone
today_start = local_tz.localize(datetime.combine(today, datetime.min.time()))
today_end = local_tz.localize(datetime.combine(today, datetime.max.time()))
tomorrow_start = local_tz.localize(datetime.combine(tomorrow, datetime.min.time()))
tomorrow_end = local_tz.localize(datetime.combine(tomorrow, datetime.max.time()))

# Read the iCalendar file
with open('/tmp/myCal.ics', 'r', encoding='latin-1') as file:
    ics_string = file.read()

# Get today's events
today_events = get_events_from_ics(ics_string, today_start, today_end)

# Get tomorrow's events
tomorrow_events = get_events_from_ics(ics_string, tomorrow_start, tomorrow_end)

# Format today's events
today_events_data = "Today:\n"
for event in today_events:
    summary = event['summary']
    if event['allday']:
        today_events_data += f"{summary}\n"
    else:
        start_time = event['startdt'].astimezone(local_tz).strftime('%H:%M')
        today_events_data += f"{summary} starts: {start_time}\n"

# Format tomorrow's events
tomorrow_events_data = "Tomorrow:\n"
for event in tomorrow_events:
    summary = event['summary']
    if event['allday']:
        tomorrow_events_data += f"{summary}\n"
    else:
        start_time = event['startdt'].astimezone(local_tz).strftime('%H:%M')
        tomorrow_events_data += f"{summary} starts: {start_time}\n"

# Combine today's and tomorrow's events data
events_data = f"{today_events_data}\n{tomorrow_events_data}"

# Get the user's home directory path
home_dir = os.path.expanduser('~')

# Read the HTML file
html_file_path = os.path.join(home_dir, '.myHomePage', 'myHomePage.html')
with open(html_file_path, 'r') as file:
    html_content = file.read()

# Replace the entire content between the zero-width space delimiters in the HTML file
start_delimiter = '\u200b'
end_delimiter = '\u200b'
start_index = html_content.find(start_delimiter)
end_index = html_content.find(end_delimiter, start_index + 1)

if start_index != -1 and end_index != -1:
    html_content = html_content[:start_index] + start_delimiter + events_data + end_delimiter + html_content[end_index + 1:]
else:
    print("Error: Zero-width space delimiters not found in the HTML file.")

# Write the updated HTML content back to the file
with open(html_file_path, 'w') as file:
    file.write(html_content)