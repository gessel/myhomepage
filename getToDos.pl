#!/usr/bin/perl

# ###################################################################
# This code should extract ToDos out of a Thunderbird calendar.sqlite
# file and write them to a .ics file. it is expected to be run
# periodically (e.g. by cron). It is expected to be part of
# small bits of code:
# getEvents.pl and getToDos.pl extract events from a 
# Thunderbird calendar .sqlite database and write them to
# ical files (which seem compliant), then putEvents.py and
# putTodos.py extract specific events and todos based on filters
# and format them and write the foramtted blocks to myHomePage.html
# which displays them, along with the time and weather and a random
# picture as a nice home page alternative to advertising or blank.
#
# Written largely by claude.ai with some inexpert human help by me.
#
# SPDX-FileCopyrightText: © 2024 David Gessel <dg@brt.llc>
# SPDX-License-Identifier: BSD-3-Clause"
# ###################################################################

use strict;
use warnings;
use DBI;
use DateTime;

# Set the path to the Thunderbird local.sqlite database file
my $db_path = "//home/<username>/.thunderbird/RaNdOm.<username>/calendar-data/cache.sqlite";

# Set the temporary database file path
my $temp_db_path = "/tmp/local_temp.sqlite";

# Set the output iCalendar file path
my $output_ical_file = "/tmp/myToDos.ics";

# Create a temporary copy of the database
system("cp $db_path $temp_db_path");

# Function to sanitize and escape special characters in the description
sub sanitize_and_escape_description {
    my ($description) = @_;
    $description //= '';  # Initialize $description to an empty string if it's undefined
    $description =~ s/[\x00-\x1F\x7F]//g;  # Remove non-printable characters
    $description =~ s/\\/\\\\/g;           # Escape backslashes
    $description =~ s/,/\\,/g;             # Escape commas
    $description =~ s/;/\\;/g;             # Escape semicolons
    $description =~ s/\\\\n/\\n/g;         # De-double-escape newlines
    return $description;
}

# Function to fold long lines
sub fold_long_lines {
    my ($text, $first_line_limit, $subsequent_line_limit) = @_;
    $first_line_limit //= 61;
    $subsequent_line_limit //= 73;

    my $folded_text = '';
    my $first_line = 1;

    while (length($text) > 0) {
        if ($first_line) {
            if (length($text) > $first_line_limit) {
                $folded_text .= substr($text, 0, $first_line_limit) . "\r\n";
                $text = substr($text, $first_line_limit);
            } else {
                $folded_text .= $text;
                $text = '';
            }
            $first_line = 0;
        } else {
            if (length($text) > $subsequent_line_limit) {
                $folded_text .= " " . substr($text, 0, $subsequent_line_limit) . "\r\n";
                $text = substr($text, $subsequent_line_limit);
            } else {
                $folded_text .= " " . $text;
                $text = '';
            }
        }
    }

    return $folded_text;
}

# Connect to the SQLite database
my $dbh = DBI->connect("dbi:SQLite:dbname=$temp_db_path", "", "", { RaiseError => 1 });

# Prepare the SQL statement
my $sql = q{
    SELECT
        t.id,
        replace(replace(replace(t.title, '|++|', '\|++|'), CHAR(10), '\n'), CHAR(13), '\n') AS title,
        t.priority,
        t.ical_status,
        t.todo_entry,
        t.todo_due,
        t.todo_completed,
        t.todo_stamp,
        r.icalString,
        replace(replace(replace(p1.value, '|++|', '\|++|'), CHAR(10), '\n'), CHAR(13), '\n') AS percent,
        replace(replace(replace(p2.value, '|++|', '\|++|'), CHAR(10), '\n'), CHAR(13), '\n') AS description,
        p3.value AS sequence,
        p4.value AS url,
        p5.value AS categories
    FROM cal_todos t
    LEFT JOIN cal_recurrence r ON t.id = r.item_id
    LEFT JOIN cal_properties p1 ON t.id = p1.item_id AND p1.key = 'PERCENT-COMPLETE'
    LEFT JOIN cal_properties p2 ON t.id = p2.item_id AND p2.key = 'DESCRIPTION'
    LEFT JOIN cal_properties p3 ON t.id = p3.item_id AND p3.key = 'SEQUENCE'
    LEFT JOIN cal_properties p4 ON t.id = p4.item_id AND p4.key = 'URL'
    LEFT JOIN cal_properties p5 ON t.id = p5.item_id AND p5.key = 'CATEGORIES'
    ORDER BY t.id, p3.value
};

my $sth = $dbh->prepare($sql);
$sth->execute();

# Open the output file for writing
open(my $fh, '>', $output_ical_file) or die "Could not open file '$output_ical_file' $!";

# Write the iCalendar header
print $fh "BEGIN:VCALENDAR\r\n";
print $fh "VERSION:2.0\r\n";
print $fh "PRODID:-//Black Rose Technology//myToDos Parser//EN\r\n";

my $prev_id = '';
my $prev_sequence = '';

while (my @row = $sth->fetchrow_array()) {
    my ($id, $title, $priority, $ical_status, $todo_entry, $todo_due, $todo_completed, $todo_stamp, $ical_string, $percent, $description, $sequence, $url, $categories) = @row;

    # Initialize undefined variables to empty strings or default values
    $id //= '';
    $title //= '';
    $priority //= '';
    $ical_status //= '';
    $todo_entry //= 0;
    $todo_due //= 0;
    $todo_completed //= 0;
    $todo_stamp //= 0;
    $percent //= '';
    $description //= '';
    $sequence //= '';
    $url //= '';
    $categories //= '';
    $ical_string //= '';

    # Skip the todo item if it has the same ID and sequence as the previous item
    if ($id eq $prev_id && $sequence eq $prev_sequence) {
        next;
    }

    $prev_id = $id;
    $prev_sequence = $sequence;

    # Convert the todo_entry, todo_due, todo_completed, and todo_stamp times to iCalendar format
    my $todo_entry_date = $todo_entry ? DateTime->from_epoch(epoch => $todo_entry / 1000000)->strftime("%Y%m%dT%H%M%SZ") : '';
    my $todo_due_date = $todo_due ? DateTime->from_epoch(epoch => $todo_due / 1000000)->strftime("%Y%m%dT%H%M%SZ") : '';
    my $todo_completed_date = $todo_completed ? DateTime->from_epoch(epoch => $todo_completed / 1000000)->strftime("%Y%m%dT%H%M%SZ") : '';
    my $todo_stamp_date = $todo_stamp ? DateTime->from_epoch(epoch => $todo_stamp / 1000000)->strftime("%Y%m%dT%H%M%SZ") : '';

    # Sanitize and escape special characters in the description
    $description = sanitize_and_escape_description($description);

    # Replace the '\|++|' back to the original separator in the title, percent, description, and categories
    $title =~ s/\\\|\+\+\|/\|\+\+\|/g;
    $percent =~ s/\\\|\+\+\|/\|\+\+\|/g;
    $description =~ s/\\\|\+\+\|/\|\+\+\|/g;
    $categories =~ s/\\\|\+\+\|/\|\+\+\|/g;

    # Remove trailing newline characters from $ical_string
    $ical_string =~ s/\r\n$//;
    $ical_string =~ s/\n$//;

    # Fold long lines
    $title = fold_long_lines($title);
    $description = fold_long_lines($description);
    $ical_string = fold_long_lines($ical_string);
    $categories = fold_long_lines($categories);

    # Generate the iCalendar todo item block
    print $fh "BEGIN:VTODO\r\n";
    print $fh "UID:$id\r\n" if $id;
    print $fh "SUMMARY:$title\r\n" if $title;
    print $fh "PRIORITY:$priority\r\n" if $priority;
    print $fh "STATUS:$ical_status\r\n" if $ical_status;
    print $fh "DTSTART:$todo_entry_date\r\n" if $todo_entry_date;
    print $fh "DUE:$todo_due_date\r\n" if $todo_due_date;
    print $fh "COMPLETED:$todo_completed_date\r\n" if $todo_completed_date;
    print $fh "DTSTAMP:$todo_stamp_date\r\n" if $todo_stamp_date;
    print $fh "PERCENT-COMPLETE:$percent\r\n" if $percent;
    print $fh "DESCRIPTION:$description\r\n" if $description;
    print $fh "SEQUENCE:$sequence\r\n" if $sequence;
    print $fh "URL:$url\r\n" if $url;
    print $fh "CATEGORIES:$categories\r\n" if $categories;
    print $fh "$ical_string\r\n" if $ical_string;
    print $fh "END:VTODO\r\n";
}

# Write the iCalendar footer
print $fh "END:VCALENDAR\r\n";

# Close the output file
close($fh);

# Disconnect from the database
$sth->finish();
$dbh->disconnect();

# Remove the temporary database file
unlink($temp_db_path);