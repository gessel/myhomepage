#!/usr/bin/perl

# ###################################################################
# This code should extract events out of a Thunderbird calendar.sqlite
# file and write them to a .ics file. it is expected to be run
# periodically (e.g. by cron). It is expected to be part of
# small bits of code:
# getEvents.pl and getToDos.pl extract events from a 
# Thunderbird calendar .sqlite database and write them to
# ical files (which seem compliant), then putEvents.py and
# putTodos.py extract specific events and todos based on filters
# and format them and write the foramtted blocks to myHomePage.html
# which displays them, along with the time and weather and a random
# picture as a nice home page alternative to advertising or blank.
#
# Written largely by claude.ai with some inexpert human help by me.
#
# SPDX-FileCopyrightText: © 2024 David Gessel <dg@brt.llc>
# SPDX-License-Identifier: BSD-3-Clause"
# ###################################################################

use strict;
use warnings;
use DBI;
use DateTime;

# Set the path to the Thunderbird local.sqlite database file
my $db_path = "/home/<username>/.thunderbird/RaNdOm.<username>/calendar-data/cache.sqlite";

# Set the temporary database file path
my $temp_db_path = "/tmp/local_temp.sqlite";

# Set the output iCalendar file path
my $output_ical_file = "/tmp/myCal.ics";

# Create a temporary copy of the database
system("cp $db_path $temp_db_path");

# Function to sanitize and escape special characters in the description
sub sanitize_and_escape_description {
    my ($description) = @_;
    $description //= '';  # Initialize $description to an empty string if it's undefined
    $description =~ s/[\x00-\x1F\x7F]//g;  # Remove non-printable characters
    $description =~ s/\\/\\\\/g;           # Escape backslashes
    $description =~ s/,/\\,/g;             # Escape commas
    $description =~ s/;/\\;/g;             # Escape semicolons
    $description =~ s/\\\\n/\\n/g;         # De-double-escape newlines
    return $description;
}

# Function to fold long lines
sub fold_long_lines {
    my ($text, $first_line_limit, $subsequent_line_limit) = @_;
    $first_line_limit //= 61;
    $subsequent_line_limit //= 73;

    my $folded_text = '';
    my $first_line = 1;

    while (length($text) > 0) {
        if ($first_line) {
            if (length($text) > $first_line_limit) {
                $folded_text .= substr($text, 0, $first_line_limit) . "\r\n";
                $text = substr($text, $first_line_limit);
            } else {
                $folded_text .= $text;
                $text = '';
            }
            $first_line = 0;
        } else {
            if (length($text) > $subsequent_line_limit) {
                $folded_text .= " " . substr($text, 0, $subsequent_line_limit) . "\r\n";
                $text = substr($text, $subsequent_line_limit);
            } else {
                $folded_text .= " " . $text;
                $text = '';
            }
        }
    }

    return $folded_text;
}

# Connect to the SQLite database
my $dbh = DBI->connect("dbi:SQLite:dbname=$temp_db_path", "", "", { RaiseError => 1 });

# Prepare the SQL statement
my $sql = q{
    SELECT
        e.id,
        replace(replace(replace(e.title, '|++|', '\|++|'), CHAR(10), '\n'), CHAR(13), '\n') AS title,
        e.event_start,
        e.event_end,
        e.event_stamp,
        e.event_start_tz,
        r.icalString,
        replace(replace(replace(p1.value, '|++|', '\|++|'), CHAR(10), '\n'), CHAR(13), '\n') AS description,
        replace(replace(replace(p2.value, '|++|', '\|++|'), CHAR(10), '\n'), CHAR(13), '\n') AS location,
        replace(replace(replace(p3.value, '|++|', '\|++|'), CHAR(10), '\n'), CHAR(13), '\n') AS categories,
        p4.value AS class,
        p5.value AS sequence,
        p6.value AS transp,
        p7.value AS url
    FROM cal_events e
    LEFT JOIN cal_recurrence r ON e.id = r.item_id
    LEFT JOIN cal_properties p1 ON e.id = p1.item_id AND p1.key = 'DESCRIPTION'
    LEFT JOIN cal_properties p2 ON e.id = p2.item_id AND p2.key = 'LOCATION'
    LEFT JOIN cal_properties p3 ON e.id = p3.item_id AND p3.key = 'CATEGORIES'
    LEFT JOIN cal_properties p4 ON e.id = p4.item_id AND p4.key = 'CLASS'
    LEFT JOIN cal_properties p5 ON e.id = p5.item_id AND p5.key = 'SEQUENCE'
    LEFT JOIN cal_properties p6 ON e.id = p6.item_id AND p6.key = 'TRANSP'
    LEFT JOIN cal_properties p7 ON e.id = p7.item_id AND p7.key = 'URL'
    ORDER BY e.id, p5.value
};

my $sth = $dbh->prepare($sql);
$sth->execute();

# Open the output file for writing
open(my $fh, '>', $output_ical_file) or die "Could not open file '$output_ical_file' $!";

# Write the iCalendar header
print $fh "BEGIN:VCALENDAR\r\n";
print $fh "VERSION:2.0\r\n";
print $fh "PRODID:-//Black Rose Technology//myToDos Parser//EN\r\n";

my $prev_id = '';
my $prev_sequence = '';

while (my @row = $sth->fetchrow_array()) {
    my ($id, $title, $start, $end, $stamp, $start_tz, $ical_string, $description, $location, $categories, $class, $sequence, $transp, $url) = @row;

    # Initialize undefined variables to empty strings or default values
    $id //= '';
    $title //= '';
    $start //= 0;
    $end //= 0;
    $stamp //= 0;
    $start_tz //= '';
    $description //= '';
    $location //= '';
    $categories //= '';
    $class //= '';
    $sequence //= '';
    $transp //= '';
    $url //= '';
    $ical_string //= '';

    # Skip the event if it has the same ID and sequence as the previous event
    if ($id eq $prev_id && $sequence eq $prev_sequence) {
        next;
    }

    $prev_id = $id;
    $prev_sequence = $sequence;

    # Convert the start, end, and stamp times to iCalendar format
    my $start_date = '';
    my $end_date = '';
    my $stamp_date = $stamp ? DateTime->from_epoch(epoch => $stamp / 1000000)->strftime("%Y%m%dT%H%M%SZ") : '';

# Determine if the event is an all-day event or local floating time
my $is_all_day = 0;
if ($start_tz && $start_tz eq 'floating') {
    my $start_time = $start ? DateTime->from_epoch(epoch => $start / 1000000)->strftime("%H%M%S") : '';
    my $end_time = $end ? DateTime->from_epoch(epoch => $end / 1000000)->strftime("%H%M%S") : '';
    $is_all_day = ($start_time eq '000000' && $end_time eq '000000');
}

if ($is_all_day) {
    $start_date = $start ? DateTime->from_epoch(epoch => $start / 1000000)->strftime("%Y%m%d") : '';
    $end_date = $end ? DateTime->from_epoch(epoch => $end / 1000000)->strftime("%Y%m%d") : '';
} else {
    if ($start_tz && $start_tz eq 'floating') {
        $start_date = $start ? DateTime->from_epoch(epoch => $start / 1000000)->strftime("%Y%m%dT%H%M%S") : '';
        $end_date = $end ? DateTime->from_epoch(epoch => $end / 1000000)->strftime("%Y%m%dT%H%M%S") : '';
    } else {
        $start_date = $start ? DateTime->from_epoch(epoch => $start / 1000000)->strftime("%Y%m%dT%H%M%SZ") : '';
        $end_date = $end ? DateTime->from_epoch(epoch => $end / 1000000)->strftime("%Y%m%dT%H%M%SZ") : '';
    }
}

    # Sanitize and escape special characters in the description
    $description = sanitize_and_escape_description($description);

    # Replace the '\|++|' back to the original separator in the title, description, location, and categories
    $title =~ s/\\\|\+\+\|/\|\+\+\|/g;
    $description =~ s/\\\|\+\+\|/\|\+\+\|/g;
    $location =~ s/\\\|\+\+\|/\|\+\+\|/g;
    $categories =~ s/\\\|\+\+\|/\|\+\+\|/g;

    # Remove trailing newline characters from $ical_string
    $ical_string =~ s/\r\n$//;
    $ical_string =~ s/\n$//;

    # Fold long lines
    $title = fold_long_lines($title);
    $description = fold_long_lines($description);
    $location = fold_long_lines($location);
    $categories = fold_long_lines($categories);
    $ical_string = fold_long_lines($ical_string);

    # Generate the iCalendar event block
    print $fh "BEGIN:VEVENT\r\n";
    print $fh "UID:$id\r\n" if $id;
    print $fh "SUMMARY:$title\r\n" if $title;
    if ($is_all_day) {
        print $fh "DTSTART;VALUE=DATE:$start_date\r\n" if $start_date;
        print $fh "DTEND;VALUE=DATE:$end_date\r\n" if $end_date;
    } else {
        print $fh "DTSTART:$start_date\r\n" if $start_date;
        print $fh "DTEND:$end_date\r\n" if $end_date;
    }
    print $fh "DTSTAMP:$stamp_date\r\n" if $stamp_date;
    print $fh "DESCRIPTION:$description\r\n" if $description;
    print $fh "LOCATION:$location\r\n" if $location;
    print $fh "CATEGORIES:$categories\r\n" if $categories;
    print $fh "CLASS:$class\r\n" if $class;
    print $fh "SEQUENCE:$sequence\r\n" if $sequence;
    print $fh "TRANSP:$transp\r\n" if $transp;
    print $fh "URL:$url\r\n" if $url;
    print $fh "$ical_string\r\n" if $ical_string;
    print $fh "END:VEVENT\r\n";
}

# Write the iCalendar footer
print $fh "END:VCALENDAR\r\n";

# Close the output file
close($fh);

# Disconnect from the database
$sth->finish();
$dbh->disconnect();

# Remove the temporary database file
unlink($temp_db_path);